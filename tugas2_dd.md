Debugging adalah salah satu metode untuk mengetahui letak kesalahan dalam suatu kode program, termasuk website. Sehingga ketika Anda mengetahui letak kesalahannya, Anda dapat memperbaiki program tersebut agar dapat berjalan dengan benar. Ada banyak cara yang dapat Anda lakukan untuk melakukan debugging.

Fungsi dd() hanya digunakan pada Framework Laravel. Jika Anda menggunakan PHP Sktruktural maupun framework PHP yang lain, dd() tidak dapat digunakan.
Fungsi dd() sangat sesuai jika Anda hanya ingin melihat isi dari 1 variabel saja. Karena setiap kali Anda menggunakan fungsi dd(), program akan berhenti tepat setelah fungsi dd() digunakan. 

Contoh.
Public function view(){
$user = 'hay laravel word';
dd($data);
$data = ['user' => $user];
Return view(‘tampil_data’, $data);
}
